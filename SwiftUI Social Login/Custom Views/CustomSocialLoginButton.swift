import SwiftUI

struct CustomSocialButton: View {
    var image: String
    var text: String
    var color: String
    var action: (() -> ())?
    
    var body: some View{
        HStack{
            Button(
                action: {
                    action?()
                },
                label: {
                    HStack{
                        Image(image)
                            .frame(width:32, height: 32)
                            .padding(.horizontal, 12)
                            .padding(.trailing,10)
                        
                        Text(text)
                            .bold()
                            .foregroundColor(Color.white)
                        
                        Spacer()
                    }
                    .frame(maxWidth: .infinity, minHeight: 50)
                    .background(RoundedRectangle(cornerRadius: 25.0).fill(Color(color)))
                })
        }
    }
}
