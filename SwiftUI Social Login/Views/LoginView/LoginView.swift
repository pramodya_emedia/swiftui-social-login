import SwiftUI

struct LoginView: View {
    @ObservedObject var vm = LoginViewModel()
    
    var body: some View {
        VStack {
            
            Text("Social Login Demo App Using SwiftUI")
                .font(.title2)
                .bold()
                .multilineTextAlignment(.center)
                .padding()
                .padding(.top, 60)
            
            Spacer()
            
            GroupBox {
                CustomSocialButton(image: "ic_login_facebook",
                                   text: "Sign in with Facebook",
                                   color: "blue_dark",
                                   action: {
                                    vm.signIn(using: .facebook)
                                   })
                    .padding(.top, 5)
                
                CustomSocialButton(image: "ic_login_google",
                                   text: "Sign in with Google",
                                   color: "blue_light",
                                   action: {
                                    vm.signIn(using: .google)
                                   })
                    .padding(.top, 5)
                
                CustomSocialButton(image: "ic_login_apple",
                                   text: "Sign in with Apple",
                                   color: "black",
                                   action: {
                                    vm.signIn(using: .apple)
                                   })
                    .padding(.top, 5)
            }
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
