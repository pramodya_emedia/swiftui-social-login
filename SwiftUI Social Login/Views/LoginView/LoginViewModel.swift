import SwiftUI
import AuthenticationServices
import FBSDKLoginKit
import GoogleSignIn

enum SocialLoginType: String {
    case facebook, google, apple
}

class LoginViewModel: NSObject, ObservableObject {
    @AppStorage("token") var token: String?
    
    override init() {
        super.init()
        
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func signIn(using type: SocialLoginType) {
        switch type {
        case .apple: performAppleSignIn()
        case .facebook: performFacebookSignIn()
        case .google: performGoogleSignIn()
        }
    }
}

// MARK: Google SignIn
extension LoginViewModel: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            guard let token = user.authentication.accessToken else {
                return
            }
            
            // MARK: TODO
            /// 1. Set token here
            /// 2. Perform tasks to do after login
            self.token = token
        } else {
            print(error.debugDescription)
        }
    }
    
    private func performGoogleSignIn() {
        if GIDSignIn.sharedInstance().currentUser == nil {
            GIDSignIn.sharedInstance().presentingViewController = UIApplication.shared.windows.first?.rootViewController
            GIDSignIn.sharedInstance().signIn()
        }
    }
}

// MARK: Apple SignIn
extension LoginViewModel: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController,
                                 didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIdCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let token = appleIdCredential.identityToken?.base64EncodedString()  else {
                return
            }
            
            // MARK: TODO
            /// 1. Set token here
            /// 2. Perform tasks to do after login
            self.token = token
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error)
    }
    
    private func performAppleSignIn() {
        let provider = ASAuthorizationAppleIDProvider()
        let request = provider.createRequest()
        request.requestedScopes = [.fullName, .email]
        let controller = ASAuthorizationController(authorizationRequests: [request])
        controller.delegate = self
        controller.performRequests()
    }
}

// MARK: Facebook SignIn
extension LoginViewModel {
    private func performFacebookSignIn() {
        let loginManager = LoginManager()
        loginManager.logIn(permissions: ["email", "public_profile"], from: nil) { result, error in
            if let fbLoginResult = result {
                
                if fbLoginResult.isCancelled {
                    return
                }
                
                guard let token = fbLoginResult.token?.tokenString else {
                    return
                }
                
                // MARK: TODO
                /// 1. Set token here
                /// 2. Perform tasks to do after login
                self.token = token
            }
        }
    }
}
