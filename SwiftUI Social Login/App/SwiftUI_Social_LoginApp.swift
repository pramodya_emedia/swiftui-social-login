import SwiftUI
import Firebase
import GoogleSignIn

@main
struct SwiftUI_Social_LoginApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var appDelegate
    @AppStorage("token") var token: String?
    
    init() {
        setupGoogleAuthentication()
    }
    
    var body: some Scene {
        WindowGroup {
            if token != nil {
                HomeView()
            } else {
                LoginView()
            }
        }
    }
}

extension SwiftUI_Social_LoginApp {
    private func setupGoogleAuthentication() {
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
    }
}
