# SwiftUI - Social Login Demo #

![SwiftUI](https://img.shields.io/badge/SwiftUI-2.0-yellow.svg)
![Xcode](https://img.shields.io/badge/Xcode-12.5-yellow.svg)
![iOS](https://img.shields.io/badge/iOS-14.0-yellow.svg)

[Related documentation](https://docs.google.com/document/d/1N4dELY0goH7M5wWqPeIsgwPdfSYjmuxS2hXeFLpvH9M/edit?usp=sharing)

## Maintainers

This project is being maintained by: _Pramodya Abeysinghe_
